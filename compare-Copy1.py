
# coding: utf-8

# In[1]:

import pandas as pd
def modify(df):
    movie=dict()
    movie['V']='Video'
    movie['C']='Clip'
    df=df.replace({"movie": movie})
    
    category=dict()
    category['A']='Adventure'
    category['S']='Scary'
    df=df.replace({"category": category})
    df.season.replace('^S','',regex=True, inplace=True)
    df['season']=pd.to_numeric(df['season'])
    df.episode.replace('^E','',regex=True, inplace=True)
    df['episode']=pd.to_numeric(df['episode'])
    #date
    try:
        df['date']=pd.to_datetime(str(df['date']),format="%Y-%m-%d",exact=True,errors='raise')
    except:
        try:
           df['date']=pd.to_datetime(df['date'],format="%m%d%y",exact=True,infer_datetime_format=False)
        except:
            print("Failed to parse date")
    
    df=df.apply(lambda x: x.astype(str).str.upper())
    df=df.sort_values(['ID']).reset_index(drop=True)
    return df    


# In[2]:


odf1=pd.read_excel('table1.xlsx')
odf2=pd.read_excel('table2.xlsx')
df1=modify(odf1)
df2=modify(odf2)


# In[3]:

df1


# In[4]:

df2


# In[5]:

dfa=df1.loc[df1['ID'].isin(df2['ID'])]


# In[6]:

dfa


# In[7]:

dfb=df2.loc[df2['ID'].isin(df1['ID'])]


# In[8]:

dfb


# In[9]:

dfna=df1.loc[~df1['ID'].isin(df2['ID'])]


# In[10]:

dfna


# In[11]:

dfnb=df2.loc[~df2['ID'].isin(df1['ID'])]


# In[12]:

dfnb


# In[13]:

dfa!=dfb


# In[14]:

dfb!=dfa


# In[15]:

dfa[dfa!=dfb]


# In[16]:

dfb[dfa!=dfb]


# In[17]:

df1!=df2


# In[18]:

df1.where(df1!=df2,df1['ID'],axis=0)


# In[19]:

df1[df1!=df2]


# In[20]:

df2!=df1


# In[21]:

df2.where(df2!=df1,df2['ID'],axis=0)


# In[22]:

df1.sort_index().sort_index(axis=1) == df2.sort_index().sort_index(axis=1)


# In[23]:

df2[df2!=df1]


# In[24]:

def report_diff(x):
    return x[0] if x[0] == x[1] else '{}->{}'.format(*x)


# In[25]:

pd.Panel(dict(df1=df1,df2=df2)).apply(report_diff, axis=0)


# In[26]:

pd.Panel(dict(df2=df1,df1=df2)).apply(report_diff, axis=0)


# In[27]:

pd.concat([df1, df2], ignore_index=True)


# In[28]:

df1.append(df2, ignore_index=True)


# In[29]:

df=pd.concat([df1, df2], ignore_index=True)


# In[30]:

df


# In[31]:

d=df.groupby(['ID','id','movie','name','category','title','season','episode','length'])


# In[32]:

d.describe()


# In[33]:

df.drop_duplicates(keep=False)


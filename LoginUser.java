package com.icici.athena.LoginServlet;

import java.io.IOException;
import java.util.Base64;

import javax.naming.ldap.LdapContext;

import com.icici.athena.controller.ConstantController;
import com.icici.athena.controller.DatabaseController;
import com.icici.athena.controller.UserController;
import com.icici.athena.ldap.LDAPUtil;
import com.icici.athena.user.User;

public class LoginUser {

	boolean isDebug = ConstantController.isDebug;

	public User getUser(String userid, String password) {
		LdapContext conn = null;

		try {
			conn = LDAPUtil.getConnection(userid, password, ConstantController.ldapDc);
			if (conn == null) {
				if (isDebug) {
					System.out.println("Connection NULL With  !!!\nConnection Details:\nHost:"
							+ ConstantController.ldapHost + "\nPort:" + ConstantController.ldapPort1 + "\nUser:"
							+ userid + "\n" + Base64.getEncoder().encodeToString(password.getBytes()) + "\nDC:"
							+ ConstantController.ldapDc);
				}
				return null;
			}

		} catch (Exception e) {
			if (isDebug) {
				System.out.println("Your EmployeeId might be locked !!!!");
			}
			if (isDebug) {
				System.out.println("LoginServlet : Redirect to : /login?msg=user id and password not matched");
			}

			return null;
		}
		LDAPUtil user = null;
		try {
			user = LDAPUtil.getUser(userid, conn);

			if (isDebug) {
				System.out.println("LoginServletUser: User Info " + user.getUserName());
			}
		} catch (Exception e) {
			if (isDebug) {
				System.out.println("Your EmployeeId might be locked !!!");
			}
			e.printStackTrace();
			return null;
		}
		User curr_user = new User();
		String user_principle = user.getUserPrincipal();
		curr_user.setUser_id(user_principle.substring(0, user_principle.indexOf("@")));
		curr_user.setUser_name(user.getUserName());
		String username = curr_user.getUser_name();
		if (isDebug) {
			System.out.println("UserName:" + username);
		}
		if (isDebug) {
			System.out.println("User common name = " + user.getCommonName());
			System.out.println("User distinguised name = " + user.getDistinguishedName());
			System.out.println("User principle = " + user.getUserPrincipal());
			System.out.println("User Given Name = " + user.getGivenName());
			System.out.println("User Name = " + user.getUserName());
		}

		User oracleuser=null;;
		try {
			oracleuser = new UserController().getUser(userid, username);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (oracleuser.getActive() != null && oracleuser.getActive().toString().toUpperCase().equals("YES")) {
			if (isDebug) {
				System.out.println("USER ALREADY ACTIVE");
			}
			return null;
			
		}
		int updateLogin = new DatabaseController().updateLoginTime(curr_user.getUser_id());
		if (isDebug) {
			System.out.println("user set and Login Time" + updateLogin);
		}

		return oracleuser;

	}
}

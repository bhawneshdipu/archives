package com.icici.athena;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import com.icici.athena.user.*;
import com.icici.athena.LoginServlet.LoginUser;
import com.icici.athena.role.Role;
@Component
public class CustomAuthenticationProvider
  implements AuthenticationProvider {
 
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
         System.out.println(name+" Auth "+password);
        if(name.indexOf("hello")>=0 && password.equals("world")){
        	List<Role> authorities=new ArrayList<Role>();
        	
        	User user=new LoginUser().getUser(name, password);
        	authorities=user.getAuthorities();
            //ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

        	System.out.println(user.getUser_name());
            return new UsernamePasswordAuthenticationToken(user, password, authorities);
             	
        }else{
        	return null;
        }
      
    }
    

    public boolean supports(Class<?> arg0) {
        return true;
    }
 
   
}

        +--------+----------+----------------------------------------------------------+-----------------------+------------------------------------------------------------------------------+----------------------------------------------+

        | Domain | Method   | URI                                                      | Name                  | Action                                                                       | Middleware                                   |

        +--------+----------+----------------------------------------------------------+-----------------------+------------------------------------------------------------------------------+----------------------------------------------+

        |        | GET|HEAD | /                                                        |                       | App\Http\Controllers\InitController@index                                    | web                                          |

        |        | GET|HEAD | _debugbar/assets/javascript                              | debugbar.assets.js    | Barryvdh\Debugbar\Controllers\AssetController@js                             | Barryvdh\Debugbar\Middleware\DebugbarEnabled |

        |        | GET|HEAD | _debugbar/assets/stylesheets                             | debugbar.assets.css   | Barryvdh\Debugbar\Controllers\AssetController@css                            | Barryvdh\Debugbar\Middleware\DebugbarEnabled |

        |        | DELETE   | _debugbar/cache/{key}/{tags?}                            | debugbar.cache.delete | Barryvdh\Debugbar\Controllers\CacheController@delete                         | Barryvdh\Debugbar\Middleware\DebugbarEnabled |

        |        | GET|HEAD | _debugbar/clockwork/{id}                                 | debugbar.clockwork    | Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork                | Barryvdh\Debugbar\Middleware\DebugbarEnabled |

        |        | GET|HEAD | _debugbar/open                                           | debugbar.openhandler  | Barryvdh\Debugbar\Controllers\OpenHandlerController@handle                   | Barryvdh\Debugbar\Middleware\DebugbarEnabled |

        |        | POST     | active/{activeId}/{requestId}/{option}/{key}             |                       | App\Http\Controllers\ActiveNegotiationController@updateActiveNegotiation     | web                                          |

        |        | GET|HEAD | active/{activeId}/{requestId}/{option}/{key}             |                       | App\Http\Controllers\ActiveNegotiationController@updateActiveNegotiationPage | web                                          |

        |        | POST     | api/addClient                                            |                       | App\Http\Controllers\API\APIController@addClient                             | api                                          |

        |        | GET|HEAD | api/addClientFields                                      |                       | App\Http\Controllers\API\APIController@addClientFields                       | api                                          |

        |        | GET|HEAD | api/getDefinition                                        |                       | App\Http\Controllers\API\APIController@getDefinition                         | api                                          |

        |        | GET|HEAD | api/user                                                 |                       | Closure                                                                      | api,auth:api                                 |

        |        | GET|HEAD | api/v1/service-requests/save                             |                       | App\Http\Controllers\Core\ServiceRequestController@save                      | web                                          |

        |        | POST     | api/v1/service-requests/save                             |                       | App\Http\Controllers\Core\ServiceRequestController@save                      | web                                          |

        |        | POST     | cancel/{activeId}/{requestId}/{option}/{key}             |                       | App\Http\Controllers\CancelNegotiationController@cancelNegotiation           | web                                          |

        |        | GET|HEAD | cancel/{activeId}/{requestId}/{option}/{key}             |                       | App\Http\Controllers\CancelNegotiationController@cancelNegotiationPage       | web                                          |

        |        | GET|HEAD | clients                                                  |                       | App\Http\Controllers\Core\ClientController@index                             | web,CheckLogin                               |

        |        | GET|HEAD | clients/add                                              |                       | App\Http\Controllers\Core\ClientController@add                               | web,CheckLogin                               |

        |        | GET|HEAD | clients/ajax-edit/{editId}                               |                       | App\Http\Controllers\Core\ClientController@ajaxEdit                          | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/add/{clientId}                       |                       | App\Http\Controllers\Core\ClientDefinitionController@add                     | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/ajax-edit/{editId}                   |                       | App\Http\Controllers\Core\ClientDefinitionController@ajaxEdit                | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/delete/{clientId}                    |                       | App\Http\Controllers\Core\ClientDefinitionController@delete                  | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/edit/{clientId}/{indexValue}         |                       | App\Http\Controllers\Core\ClientDefinitionController@edit                    | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/filter/{clientId}                    |                       | App\Http\Controllers\Core\ClientDefinitionController@filter                  | web,CheckLogin                               |

        |        | POST     | clients/definitions/save                                 |                       | App\Http\Controllers\Core\ClientDefinitionController@save                    | web,CheckLogin                               |

        |        | POST     | clients/definitions/save/save                            |                       | App\Http\Controllers\Core\ClientDefinitionController@save                    | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/view/{clientId}                      |                       | App\Http\Controllers\Core\ClientDefinitionController@view                    | web,CheckLogin                               |

        |        | GET|HEAD | clients/definitions/{clientId}                           |                       | App\Http\Controllers\Core\ClientDefinitionController@index                   | web,CheckLogin                               |

        |        | GET|HEAD | clients/delete/{editId}                                  |                       | App\Http\Controllers\Core\ClientController@delete                            | web,CheckLogin                               |

        |        | GET|HEAD | clients/edit/{editId}                                    |                       | App\Http\Controllers\Core\ClientController@edit                              | web,CheckLogin                               |

        |        | GET|HEAD | clients/filter                                           |                       | App\Http\Controllers\Core\ClientController@filter                            | web,CheckLogin                               |

        |        | GET|HEAD | clients/save                                             |                       | App\Http\Controllers\Core\ClientController@save                              | web,CheckLogin                               |

        |        | POST     | clients/save                                             |                       | App\Http\Controllers\Core\ClientController@save                              | web,CheckLogin                               |

        |        | GET|HEAD | clients/view/{editId}                                    |                       | App\Http\Controllers\Core\ClientController@view                              | web,CheckLogin                               |

        |        | GET|HEAD | dashboard                                                |                       | App\Http\Controllers\InitController@dashboard                                | web,CheckLogin                               |

        |        | GET|HEAD | definitions                                              |                       | App\Http\Controllers\Core\DefinitionController@index                         | web,CheckLogin                               |

        |        | GET|HEAD | definitions/add                                          |                       | App\Http\Controllers\Core\DefinitionController@add                           | web,CheckLogin                               |

        |        | GET|HEAD | definitions/ajax-edit/{editId}                           |                       | App\Http\Controllers\Core\DefinitionController@ajaxEdit                      | web,CheckLogin                               |

        |        | GET|HEAD | definitions/delete/{editId}                              |                       | App\Http\Controllers\Core\DefinitionController@delete                        | web,CheckLogin                               |

        |        | GET|HEAD | definitions/edit/{editId}                                |                       | App\Http\Controllers\Core\DefinitionController@edit                          | web,CheckLogin                               |

        |        | GET|HEAD | definitions/filter                                       |                       | App\Http\Controllers\Core\DefinitionController@filter                        | web,CheckLogin                               |

        |        | POST     | definitions/save                                         |                       | App\Http\Controllers\Core\DefinitionController@save                          | web,CheckLogin                               |

        |        | GET|HEAD | definitions/view/{editId}                                |                       | App\Http\Controllers\Core\DefinitionController@view                          | web,CheckLogin                               |

        |        | GET|HEAD | error                                                    |                       | App\Http\Controllers\InitController@error                                    | web                                          |

        |        | POST     | login                                                    |                       | App\Http\Controllers\InitController@login                                    | web                                          |

        |        | GET|HEAD | login                                                    |                       | App\Http\Controllers\InitController@index                                    | web                                          |

        |        | GET|HEAD | logout                                                   |                       | App\Http\Controllers\InitController@logout                                   | web                                          |

        |        | GET|HEAD | retry_queued_requests                                    |                       | Closure                                                                      | web                                          |

        |        | GET|HEAD | retryfailedjobs                                          |                       | Closure                                                                      | web                                          |

        |        | GET|HEAD | service-requests/service-request-logs                    |                       | App\Http\Controllers\Core\ServiceRequestLogController@index                  | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-request-logs/add                |                       | App\Http\Controllers\Core\ServiceRequestLogController@add                    | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-request-logs/ajax-edit/{editId} |                       | App\Http\Controllers\Core\ServiceRequestLogController@ajaxEdit               | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-request-logs/delete/{editId}    |                       | App\Http\Controllers\Core\ServiceRequestLogController@delete                 | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-request-logs/edit/{editId}      |                       | App\Http\Controllers\Core\ServiceRequestLogController@edit                   | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-request-logs/filter             |                       | App\Http\Controllers\Core\ServiceRequestLogController@filter                 | web,CheckLogin                               |

        |        | POST     | service-requests/service-request-logs/save               |                       | App\Http\Controllers\Core\ServiceRequestLogController@save                   | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-request-logs/view/{editId}      |                       | App\Http\Controllers\Core\ServiceRequestLogController@view                   | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests                        |                       | App\Http\Controllers\Core\ServiceRequestController@index                     | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests/add                    |                       | App\Http\Controllers\Core\ServiceRequestController@add                       | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests/ajax-edit/{editId}     |                       | App\Http\Controllers\Core\ServiceRequestController@ajaxEdit                  | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests/delete/{editId}        |                       | App\Http\Controllers\Core\ServiceRequestController@delete                    | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests/edit/{editId}          |                       | App\Http\Controllers\Core\ServiceRequestController@edit                      | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests/filter                 |                       | App\Http\Controllers\Core\ServiceRequestController@filter                    | web,CheckLogin                               |

        |        | POST     | service-requests/service-requests/save                   |                       | App\Http\Controllers\Core\ServiceRequestController@save                      | web,CheckLogin                               |

        |        | GET|HEAD | service-requests/service-requests/view/{editId}          |                       | App\Http\Controllers\Core\ServiceRequestController@view                      | web,CheckLogin                               |

        |        | GET|HEAD | simulators/generate-service-request-form                 |                       | App\Http\Controllers\Core\SimulatorController@generateServiceRequestForm     | web,CheckLogin                               |

        |        | GET|HEAD | simulators/provisioning-api                              |                       | App\Http\Controllers\Core\SimulatorController@renderDefinitionForm           | web,CheckLogin                               |

        |        | GET|HEAD | simulators/service-request-api                           |                       | App\Http\Controllers\Core\SimulatorController@renderServiceRequestForm       | web,CheckLogin                               |

        |        | GET|HEAD | system/admins                                            |                       | App\Http\Controllers\Core\AdminController@index                              | web,CheckLogin                               |

        |        | GET|HEAD | system/admins/add                                        |                       | App\Http\Controllers\Core\AdminController@add                                | web,CheckLogin                               |

        |        | GET|HEAD | system/admins/delete/{editId}                            |                       | App\Http\Controllers\Core\AdminController@delete                             | web,CheckLogin                               |

        |        | GET|HEAD | system/admins/edit/{editId}                              |                       | App\Http\Controllers\Core\AdminController@edit                               | web,CheckLogin                               |

        |        | POST     | system/admins/save                                       |                       | App\Http\Controllers\Core\AdminController@save                               | web,CheckLogin                               |

        |        | GET|HEAD | system/admins/view/{editId}                              |                       | App\Http\Controllers\Core\AdminController@view                               | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes                                       |                       | App\Http\Controllers\Core\MatchModeController@index                          | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes/add                                   |                       | App\Http\Controllers\Core\MatchModeController@add                            | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes/ajax-edit/{editId}                    |                       | App\Http\Controllers\Core\MatchModeController@ajaxEdit                       | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes/delete/{editId}                       |                       | App\Http\Controllers\Core\MatchModeController@delete                         | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes/edit/{editId}                         |                       | App\Http\Controllers\Core\MatchModeController@edit                           | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes/filter                                |                       | App\Http\Controllers\Core\MatchModeController@filter                         | web,CheckLogin                               |

        |        | POST     | system/match-modes/save                                  |                       | App\Http\Controllers\Core\MatchModeController@save                           | web,CheckLogin                               |

        |        | GET|HEAD | system/match-modes/view/{editId}                         |                       | App\Http\Controllers\Core\MatchModeController@view                           | web,CheckLogin                               |

        |        | GET|HEAD | system/operators                                         |                       | App\Http\Controllers\Core\OperatorController@index                           | web,CheckLogin                               |

        |        | GET|HEAD | system/operators/add                                     |                       | App\Http\Controllers\Core\OperatorController@add                             | web,CheckLogin                               |

        |        | GET|HEAD | system/operators/ajax-edit/{editId}                      |                       | App\Http\Controllers\Core\OperatorController@ajaxEdit                        | web,CheckLogin                               |

        |        | GET|HEAD | system/operators/delete/{editId}                         |                       | App\Http\Controllers\Core\OperatorController@delete                          | web,CheckLogin                               |

        |        | GET|HEAD | system/operators/edit/{editId}                           |                       | App\Http\Controllers\Core\OperatorController@edit                            | web,CheckLogin                               |

        |        | GET|HEAD | system/operators/filter                                  |                       | App\Http\Controllers\Core\OperatorController@filter                          | web,CheckLogin                               |

        |        | POST     | system/operators/save                                    |                       | App\Http\Controllers\Core\OperatorController@save                            | web,CheckLogin                               |

        |        | GET|HEAD | system/operators/view/{editId}                           |                       | App\Http\Controllers\Core\OperatorController@view                            | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings                                   |                       | App\Http\Controllers\Core\SystemSettingController@index                      | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings/add                               |                       | App\Http\Controllers\Core\SystemSettingController@add                        | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings/ajax-edit/{editId}                |                       | App\Http\Controllers\Core\SystemSettingController@ajaxEdit                   | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings/delete/{editId}                   |                       | App\Http\Controllers\Core\SystemSettingController@delete                     | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings/edit/{editId}                     |                       | App\Http\Controllers\Core\SystemSettingController@edit                       | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings/filter                            |                       | App\Http\Controllers\Core\SystemSettingController@filter                     | web,CheckLogin                               |

        |        | POST     | system/system-settings/save                              |                       | App\Http\Controllers\Core\SystemSettingController@save                       | web,CheckLogin                               |

        |        | GET|HEAD | system/system-settings/view/{editId}                     |                       | App\Http\Controllers\Core\SystemSettingController@view                       | web,CheckLogin                               |

        +--------+----------+----------------------------------------------------------+-----------------------+------------------------------------------------------------------------------+----------------------------------------------+
